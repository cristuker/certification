package net.openid.conformance.raidiam.validators.openIDProvider;

import net.openid.conformance.logging.ApiName;

/**
 * Api url: ****
 * Api endpoint: POST /token/introspection
 * Api git hash: ****
 *
 */
@ApiName("Raidiam Directory POST Token Introspection")
public class PostTokenIntrospectionValidator extends PostBackChannelValidator {
}
