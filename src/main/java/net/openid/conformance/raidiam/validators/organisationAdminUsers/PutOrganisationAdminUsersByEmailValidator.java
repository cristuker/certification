package net.openid.conformance.raidiam.validators.organisationAdminUsers;

import net.openid.conformance.logging.ApiName;

/**
 * This class corresponds to {@link PostOrganisationAdminUsersValidator}
 * Api url: ****
 * Api endpoint: PUT /organisations/{OrganisationId}/adminusers
 * Api git hash: ****
 *
 */
@ApiName("Raidiam Directory PUT Organisation Admin Users ByEmail")
public class PutOrganisationAdminUsersByEmailValidator extends PostOrganisationAdminUsersValidator {
}
