package net.openid.conformance.raidiam.validators.authorisationServers.base;

import net.openid.conformance.logging.ApiName;

/**
 * Api endpoint: GET /organisations/{OrganisationId}/authorisationservers/{AuthorisationServerId}
 */
@ApiName("Raidiam Directory PUT Authorisation Servers Base by serverId")
public class PutAuthorisationServersByServerIdValidator extends PostAuthorisationServersValidator {
}
