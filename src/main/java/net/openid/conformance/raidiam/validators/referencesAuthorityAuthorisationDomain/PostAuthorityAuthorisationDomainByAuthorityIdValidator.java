package net.openid.conformance.raidiam.validators.referencesAuthorityAuthorisationDomain;

import net.openid.conformance.logging.ApiName;

/**
 * Api url: ****
 * Api endpoint: POST /references/authorities/{AuthorityId}/authorisationdomains
 * Api git hash: ****
 *
 */
@ApiName("Raidiam Directory POST Authority Authorisation Domain By AuthorityId")
public class PostAuthorityAuthorisationDomainByAuthorityIdValidator extends GetAuthorityAuthorisationDomainValidator {
}
