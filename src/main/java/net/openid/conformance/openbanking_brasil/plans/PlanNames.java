package net.openid.conformance.openbanking_brasil.plans;

public class PlanNames {
	/** VERSION 1 **/
	/* Phase 1 - Open Data */
	public static final String ADMIN_API_TEST_PLAN  = "Functional tests for Admin API - based on Swagger version: 1.0.1 (WIP)";

	public static final String BANKING_AGENTS_API_TEST_PLAN = "Functional tests for Channels - BankingAgents API - based on Swagger version: 1.0.3 (WIP)";
	public static final String BRANCHES_API_TEST_PLAN = "Functional tests for Channels - Branches API - based on Swagger version: 1.0.3 (WIP)";
	public static final String ELECTRONIC_CHANNELS_API_TEST_PLAN = "Functional tests for Channels - Electronic Channels API - based on Swagger version: 1.0.3 (WIP)";
	public static final String PHONE_CHANNELS_API_TEST_PLAN = "Functional tests for Channels - Phone Channels API - based on Swagger version: 1.0.3 (WIP)";
	public static final String SHARED_AUTOMATED_TELLER_MACHINES_API_TEST_PLAN = "Functional tests for Channels - Shared Automated Teller Machines API - based on Swagger version: 1.0.3 (WIP)";

	public static final String COMMON_API_TEST_PLAN  = "Functional tests for Common API - based on Swagger version: 1.0.2 (WIP)";

	public static final String BUSINESS_ACCOUNTS_API_TEST_PLAN = "Functional tests for ProductsNServices - BusinessAccounts API - based on Swagger version: 1.0.0 (WIP)";
	public static final String BUSINESS_CREDIT_CARD_API_TEST_PLAN = "Functional tests for ProductsNServices - BusinessCreditCard API - based on Swagger version: 1.0.0 (WIP)";
	public static final String BUSINESS_FINANCINGS_API_TEST_PLAN = "Functional tests for ProductsNServices - BusinessFinancings API - based on Swagger version: 1.0.0 (WIP)";
	public static final String BUSINESS_INVOICE_FINANCINGS_API_TEST_PLAN = "Functional tests for ProductsNServices - BusinessInvoiceFinancings API - based on Swagger version: 1.0.0 (WIP)";
	public static final String BUSINESS_LOANS_API_TEST_PLAN = "Functional tests for ProductsNServices - BusinessLoans API - based on Swagger version: 1.0.0 (WIP)";
	public static final String PERSONAL_ACCOUNTS_API_TEST_PLAN = "Functional tests for ProductsNServices - PersonalAccounts API - based on Swagger version: 1.0.0 (WIP)";
	public static final String PERSONAL_CREDIT_CARD_API_TEST_PLAN = "Functional tests for ProductsNServices - PersonalCreditCard API - based on Swagger version: 1.0.0 (WIP)";
	public static final String PERSONAL_FINANCINGS_API_TEST_PLAN = "Functional tests for ProductsNServices - PersonalFinancings API - based on Swagger version: 1.0.0 (WIP)";
	public static final String PERSONAL_INVOICE_FINANCINGS_API_TEST_PLAN = "Functional tests for ProductsNServices - PersonalInvoiceFinancings API - based on Swagger version: 1.0.0 (WIP)";
	public static final String PERSONAL_LOANS_API_TEST_PLAN = "Functional tests for ProductsNServices - PersonalLoans API - based on Swagger version: 1.0.0 (WIP)";
	public static final String UNARRANGED_ACCOUNT_BUSINESS_OVERDRAFT_API_TEST_PLAN = "Functional tests for ProductsNServices - UnarrangedAccountBusinessOverdraft API - based on Swagger version: 1.0.0 (WIP)";
	public static final String UNARRANGED_ACCOUNT_PERSONAL_OVERDRAFT_API_TEST_PLAN = "Functional tests for ProductsNServices - UnarrangedAccountPersonalOverdraft API - based on Swagger version: 1.0.0 (WIP)";

	/* Phase 2 - Customer Data */

	public static final String ACCOUNT_API_NAME = "Functional tests for accounts API - based on Swagger version: 1.0.3";
	public static final String CONSENTS_API_NAME = "Functional tests for consents API - based on Swagger version: 1.0.3";
	public static final String CREDIT_CARDS_API_PLAN_NAME = "Functional tests for Credit Card API - based on swagger version: 1.0.4";
	public static final String CREDIT_OPERATIONS_API_PLAN_NAME = "Functional tests for credit operations API - based on Swagger version: 1.0.4";
	public static final String CUSTOMER_PERSONAL_DATA_API_PLAN_NAME = "Functional tests for personal customer data API - based on Swagger version: 1.0.3";
	public static final String CUSTOMER_BUSINESS_DATA_API_PLAN_NAME = "Functional tests for business customer data API - based on Swagger version: 1.0.3";
	public static final String RESOURCES_API_PLAN_NAME = "Functional tests for resources API - based on Swagger version: 1.0.2";
	public static final String CREDIT_OPERATIONS_ADVANCES_API_PLAN_NAME = "Functional tests for unarranged overdraft API - based on Swagger version: 1.0.4";
	public static final String LOANS_API_PLAN_NAME = "Functional tests for loans API - based on Swagger version: 1.0.4";
	public static final String FINANCINGS_API_NAME = "Functional tests for financings API - based on Swagger version: 1.0.4";

	/* Phase 3 - Payment Initiation */

	public static final String OBB_DCR = "Open Banking Brazil - DCR";
	public static final String OBB_DCR_WITHOUT_BROWSER_INTERACTION_TEST_PLAN = "Brazil DCR Test without Browser Interaction";

	public static final String PAYMENTS_API_PHASE_2_TEST_PLAN = "Functional tests for payments API INIC, DICT, MANU, QRES and QRDN (T0/T1/T2) - Based on Swagger version: 1.0.1";
	public static final String PAYMENTS_API_PHASE_3_TEST_PLAN = "Functional tests for payments PIX Scheduling (T3) - Based on Swagger version: 1.1.0 (WIP)";


	public static final String CREDIT_OPERATIONS_DISCOUNTED_CREDIT_RIGHTS_API_PLAN_NAME = "Functional tests for discounted credit rights API - based on Swagger version: 1.0.4";
	public static final String PAYMENTS_API_PHASE_1_TEST_PLAN = "Functional tests for payments API INIC, DICT and MANU (T0/T1) - Based on Swagger version: 1.0.1 - Limit Submission Date 14/01";
	public static final String PRODUCTS_N_SERVICES_API_TEST_PLAN  = "Functional tests for " +
		"ProductsNServices API - based on Swagger version: 1.0.0";
	public static final String PRODUCTS_N_SERVICES_PERSON_API_TEST_PLAN  = "Functional tests for ProductsNServices - Person API - based on Swagger version: 1.0.3"; // OPIN Person
	public static final String PERSON_PENSION_PLAN_API_TEST_PLAN = "Functional tests for ProductsNServices - Pension Plan API - based on Swagger version: 1.0.2"; // OPIN Pension Plan
	public static final String AUTO_INSURANCE_PLAN_API_TEST_PLAN = "Functional tests for ProductsNServices - Auto Insurance API - based on Swagger version: 1.0.2"; // OPIN Auto Insurance
	public static final String HOME_INSURANCE_PLAN_API_TEST_PLAN = "Functional tests for ProductsNServices - Home Insurance API - based on Swagger version: 1.0.2"; // OPIN Home Insurance
	public static final String CAPITALIZATION_TITLE_PLAN_API_TEST_PLAN = "Functional tests for ProductsNServices - Capitalization Title API - based on Swagger version: 1.0.2"; // OPIN Capitalization Title
	public static final String LIFE_PENSION_PLAN_API_TEST_PLAN = "Functional tests for ProductsNServices - Life Pension API - based on Swagger version: 1.0.3"; // OPIN Life Pension
	public static final String OPIN_CHANNELS_BRANCHES_API_TEST_PLAN = "Functional tests for Channels - Branches API - based on Swagger version: 1.0.2"; // OPIN Channel Branches
	public static final String OPIN_ELECTRONIC_CHANNELS_API_TEST_PLAN = "Functional tests for Channels - Electronic Channels API - based on Swagger version: 1.0.2"; // OPIN Electronic Channel
	public static final String OPIN_PHONE_CHANNELS_API_TEST_PLAN = "Functional tests for Channels - Phone Channels API - based on Swagger version: 1.0.2"; // OPIN Phone Channels
	public static final String OPIN_ADMIN_API_TEST_PLAN = "Functional tests for Admin API - based on Swagger version: 1.0.2"; // OPIN Admin
	public static final String OPIN_DISCOVERY_TEST_PLAN = "Functional tests for Discovery API - based on Swagger version: 1.0.0"; // OPIN Discovery
	public static final String OPIN_DISCOVERY_STATUS_TEST_PLAN = "Functional tests for Discovery Status API - based on Swagger version: 1.0.0"; // OPIN Discovery
	public static final String OPIN_DISCOVERY_OUTAGES_TEST_PLAN = "Functional tests for Discovery  Outages API - based on Swagger version: 1.0.0"; // OPIN Discovery
	public static final String BUSINESS_API_TEST_PLAN = "Functional tests for ProductsServices - Business API - based on Swagger version: 1.0.0";
	public static final String AUTO_EXTENDED_WARRANTY_API_TEST_PLAN = "Functional tests for ProductsServices - Auto Extended Warranty API - based on Swagger version: 1.0.0";
	public static final String ASSISTANCE_GENERAL_ASSETS_API_TEST_PLAN = "Functional tests for ProductsServices - Assistance General Assets API - based on Swagger version: 1.0.0";
	public static final String GENERAL_LIABILITY_API_TEST_PLAN = "Functional tests for ProductsServices - General Liability API - based on Swagger version: 1.0.0";
	public static final String EQUIPMENT_BREAKDOWN_API_TEST_PLAN = "Functional tests for ProductsServices - Equipment Breakdown API - based on Swagger version: 1.0.0";
	public static final String LOST_PROFIT_API_TEST_PLAN = "Functional tests for ProductsServices - Lost Profit API - based on Swagger version: 1.0.0";
	public static final String ENGINEERING_API_TEST_PLAN = "Functional tests for ProductsServices - Engineering API - based on Swagger version: 1.0.0";
	public static final String PRIVATE_GUARANTEE_API_TEST_PLAN = "Functional tests for ProductsServices - Private Guarantee API - based on Swagger version: 1.0.0";
	public static final String DOMESTIC_CREDIT_API_TEST_PLAN = "Functional tests for ProductsServices - Domestic Credit API - based on Swagger version: 1.0.0";
	public static final String EXTENDED_WARRANTY_API_TEST_PLAN = "Functional tests for ProductsServices - Extended Warranty API - based on Swagger version: 1.0.0";
	public static final String CYBER_RISK_API_TEST_PLAN = "Functional tests for ProductsServices - Cyber Risk Plan API - based on Swagger version: 1.0.2";
	public static final String EXPORT_CREDIT_API_TEST_PLAN = "Functional tests for ProductsServices - Export Credit API - based on Swagger version: 1.0.0";
	public static final String PUBLIC_GUARANTEE_API_TEST_PLAN = "Functional tests for ProductsServices - Public Guarantee API - based on Swagger version: 1.0.0";
	public static final String NAMED_OPERATIONAL_RISKS_API_TEST_PLAN = "Functional tests for ProductsServices - Named Operational Risks API - based on Swagger version: 1.0.0";
	public static final String FINANCIAL_RISK_API_TEST_PLAN = "Functional tests for ProductsServices - Financial Risk API - based on Swagger version: 1.0.0";
	public static final String GLOBAL_BANKING_API_TEST_PLAN = "Functional tests for ProductsServices - Global Banking API - based on Swagger version: 1.0.0";
	public static final String STOP_LOSS_API_TEST_PLAN = "Functional tests for ProductsServices - Stop Loss API - based on Swagger version: 1.0.0";
	public static final String RENT_GUARANTEE_API_TEST_PLAN = "Functional tests for ProductsServices - Rent Guarantee API - based on Swagger version: 1.0.0";
	public static final String ENVIRONMENT_VARIABLE_API_TEST_PLAN = "Functional tests for ProductsServices - Environmental Liability API - based on Swagger version: 1.0.0";
	public static final String CONDOMINIUM_API_TEST_PLAN =  "Functional tests for ProductsServices - Condominium API - based on Swagger version: 1.0.0";
	public static final String ERRORS_OMISSIONS_LIABILITY_API_TEST_PLAN = "Functional tests for ProductsServices - Errors Omissions Liability API - based on Swagger version: 1.0.0";
	public static final String DIRECTORS_OFFICERS_LIABILITY_API_TEST_PLAN = "Functional tests for ProductsServices - Directors Officers Liability API - based on Swagger version: 1.0.0";

	public static final String PAYMENTS_API_ALL_TEST_PLAN = "Functional tests for payments API INIC, DICT, MANU, QRES, QRDN, Scheduling (T0/T1/T2/T3) - Based on Swagger version: 1.1.0 (WIP)";

	/** VERSION 2 **/
	public static  final String LATEST_VERSION_2 = "2.0.0-r.c1.0 (WIP)";

	public static final String ACCOUNT_API_NAME_V2 = "Functional tests for accounts API - based on Swagger version: " + LATEST_VERSION_2;
	public static final String CONSENTS_API_NAME_V2 = "Functional tests for consents API - based on Swagger version: " + LATEST_VERSION_2;
	public static final String CREDIT_CARDS_API_PLAN_NAME_V2 = "Functional tests for Credit Card API - based on swagger version: " + LATEST_VERSION_2;
	public static final String CREDIT_OPERATIONS_DISCOUNTED_CREDIT_RIGHTS_API_PLAN_NAME_V2 = "Functional tests for discounted credit rights API - based on Swagger version: " + LATEST_VERSION_2;
	public static final String CUSTOMER_PERSONAL_DATA_API_PLAN_NAME_V2 = "Functional tests for personal customer data API - based on Swagger version: " + LATEST_VERSION_2;
	public static final String CUSTOMER_BUSINESS_DATA_API_PLAN_NAME_V2 = "Functional tests for business customer data API - based on Swagger version: " + LATEST_VERSION_2;
	public static final String RESOURCES_API_PLAN_NAME_V2 = "Functional tests for resources API - based on Swagger version: " + LATEST_VERSION_2;
	public static final String CREDIT_OPERATIONS_ADVANCES_API_PLAN_NAME_V2 = "Functional tests for unarranged overdraft API - based on Swagger version: " + LATEST_VERSION_2;
	public static final String LOANS_API_PLAN_NAME_V2 = "Functional tests for loans API - based on Swagger version: " + LATEST_VERSION_2;
	public static final String FINANCINGS_API_NAME_V2 = "Functional tests for financings API - based on Swagger version: " + LATEST_VERSION_2;

	public static final String HOUSING_API_TEST_PLAN = "Functional tests for ProductsServices - Housing API - based on Swagger version: 1.0.0";
    public static final String OTHERS_SCOPES_API_TEST_PLAN = "Functional tests for ProductsServices - Others Scopes API - based on Swagger version: 1.0.0";
    public static final String RURAL_API_TEST_PLAN = "Functional tests for ProductsServices - Rural API - based on Swagger version: 1.0.0";
    public static final String TRANSPORT_API_TEST_PLAN = "Functional tests for ProductsServices - Transport API - based on Swagger version: 1.0.0";

	public static final String INTERMEDIARY_API_TEST_PLAN = "Functional tests for Channels - Intermediary API - based on Swagger version: 1.1.0";
	public static final String REFERENCED_NETWORK_API_TEST_PLAN = "Functional tests for Channels - Referenced Network API - based on Swagger version: 1.1.0";

}
