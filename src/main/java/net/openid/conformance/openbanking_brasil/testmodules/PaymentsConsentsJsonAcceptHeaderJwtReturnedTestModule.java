package net.openid.conformance.openbanking_brasil.testmodules;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.FAPIBrazilCreatePaymentConsentRequest;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.paymentInitiation.PaymentInitiationConsentValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.*;
import net.openid.conformance.openbanking_brasil.testmodules.support.payments.SanitiseQrCodeConfig;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;
import org.springframework.http.HttpStatus;

@PublishTestModule(
	testName = "payments-consents-json-accept-header-jwt-returned-test",
	displayName = "Payments Consents API test module which sends an accept header of JSON and expects a JWT",
	summary = "Payments Consents API test module which sends an accept header of JSON and expects status a JWT" +
		"Flow:" +
		"Makes a good consent flow - expects success. Calls the self endpoint with a JSON accept header and ensures a JWT is still returned." +
		"Required:" +
		"Consent url pointing at the consent endpoint.",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl"
	}
)
public class PaymentsConsentsJsonAcceptHeaderJwtReturnedTestModule extends AbstractClientCredentialsGrantFunctionalTestModule {

	@Override
	protected ConditionSequence createGetAccessTokenWithClientCredentialsSequence(Class<? extends ConditionSequence> clientAuthSequence) {
		return new ObtainPaymentsAccessTokenWithClientCredentials(clientAuthSequence);
	}

	@Override
	protected void postConfigure(JsonObject config, String baseUrl, String externalUrlOverride) {
		callAndContinueOnFailure(SanitiseQrCodeConfig.class);
	}

	@Override
	protected void runTests() {
		runInBlock("Create a payment consent", () -> {
			eventLog.startBlock("Setting date to today");
			callAndStopOnFailure(EnsurePaymentDateIsToday.class);
			callAndStopOnFailure(PrepareToPostConsentRequest.class);
			callAndStopOnFailure(FAPIBrazilCreatePaymentConsentRequest.class);

			call(sequence(SignedPaymentConsentSequence.class));

			callAndStopOnFailure(PaymentInitiationConsentValidator.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(EnsureResponseHasLinks.class, Condition.ConditionResult.FAILURE);
			callAndContinueOnFailure(ValidateResponseMetaData.class, Condition.ConditionResult.FAILURE);

			ConditionSequence validationSequence = new ValidateSelfEndpoint()
				.replace(EnsureResponseCodeWas200.class, condition(EnsureResponseCodeWas200or406.class))
				.skip(SaveOldValues.class, "Skipped in the sequence for further check")
				.skip(LoadOldValues.class, "Skipped in the sequence for further check")
				.skip(ValidateResponseMetaData.class, "Skipped in the sequence for further check");

			call(validationSequence);
			if(env.getInteger("resource_endpoint_response_full", "status") == HttpStatus.OK.value()) {
				callAndStopOnFailure(ValidateResponseMetaData.class);
				callAndContinueOnFailure(EnsureResponseWasJwt.class);
			} else {
				callAndStopOnFailure(ValidateErrorFromResourceEndpointResponseError.class);
			}

		});
	}
}
