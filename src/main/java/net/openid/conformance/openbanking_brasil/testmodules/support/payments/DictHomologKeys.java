package net.openid.conformance.openbanking_brasil.testmodules.support.payments;

public class DictHomologKeys {
	public static final String PROXY_EMAIL = "cliente-a00001@pix.bcb.gov.br";
	public static final String PROXY_EMAIL_BANK_CODE = "998";
	public static final String PROXY_EMAIL_ACCOUNT_NUMBER = "12345678";
	public static final String PROXY_EMAIL_BRANCH_NUMBER = "0001";
	public static final String PROXY_EMAIL_ISPB = "99999004";
	public static final String PROXY_EMAIL_CPF = "99991111140";
	public static final String PROXY_EMAIL_OWNER_NAME= "Joao Silva";
	public static final String PROXY_EMAIL_PERSON_TYPE = "PESSOA_NATURAL";
	public static final String PROXY_EMAIL_ACCOUNT_TYPE = "CACC";
	public static final String PROXY_EMAIL_STANDARD_TYPE = "PIX";
	public static final String PROXY_EMAIL_STANDARD_AMOUNT = "100.00";
	public static final String PROXY_EMAIL_STANDARD_CURRENCY = "BRL";
	public static final String PROXY_EMAIL_STANDARD_LOCALINSTRUMENT = "DICT";
	public static final String PROXY_EMAIL_STANDARD_IBGETOWNCODE = "5300108";
	public static final String PROXY_EMAIL_STANDARD_CODE = "OTHER";
	public static final String PROXY_EMAIL_STANDARD_ADDITIONALINFORMATION = "Conformance Suite Test";
	public static final String PROXY_EMAIL_STANDARD_REMITTANCEINFORMATION ="Conformance Suite Test";
	public static final String PROXY_QRDN_EDIT_PAYMENT_REMITTANCE_INFORMATION ="$$EDITPAYMENT$$:20201.00";
	public static final String PROXY_TRANSACTION_IDENTIFICATION = "E00038166201907261559y6j6";
	public static final String PROXY_PHONE_NUMBER = "+5561990010001";
	public static final String PROXY_PHONE_NUMBER_BANK_CODE = "998";
	public static final String PROXY_PHONE_NUMBER_ACCOUNT_NUMBER = "12345678";
	public static final String PROXY_PHONE_NUMBER_BRANCH_NUMBER = "0001";
	public static final String PROXY_PHONE_NUMBER_ISPB = "99999004";
	public static final String PROXY_PHONE_NUMBER_CPF = "11112222235";
	public static final String PROXY_PHONE_NUMBER_OWNER_NAME = "Joao Silva";
	public static final String PROXY_PHONE_NUMBER_PERSON_TYPE = "PESSOA_NATURAL";
	public static final String PROXY_PHONE_NUMBER_ACCOUNT_TYPE = "CACC";
}
