package net.openid.conformance.openbanking_brasil.testmodules.support;

public class ResourceErrorMetaValidator extends ErrorMetaValidator{
	@Override
	protected boolean isResource() {
		return true;
	}
}
