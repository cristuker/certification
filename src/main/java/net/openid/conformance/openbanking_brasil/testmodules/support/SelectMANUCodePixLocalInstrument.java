package net.openid.conformance.openbanking_brasil.testmodules.support;

public class SelectMANUCodePixLocalInstrument extends AbstractPixLocalInstrumentCondition {
	@Override
	protected String getPixLocalInstrument() {
		return "MANU";
	}
}
